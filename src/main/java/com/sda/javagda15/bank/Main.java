package com.sda.javagda15.bank;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        for (int i = 0; i < 10000; i++) {
            bank.przelewPrzychodzący(10);
            bank.przelewWychodzący(10);
        }

        Scanner scanner = new Scanner(System.in);
        String linia;
        do{
            linia = scanner.nextLine();

            bank.wypiszStanKonta();
        }while (!linia.equalsIgnoreCase("quit"));
    }
}
