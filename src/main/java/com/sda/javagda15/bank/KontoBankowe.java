package com.sda.javagda15.bank;

public class KontoBankowe {
    private Double stanKonta;
    private final Object lock = new Object();

    public KontoBankowe() {
        this.stanKonta = 0.0;
    }

    public void przelewPrzychodzący(double kwota) {
//        synchronized (lock) {
            this.stanKonta += kwota;
//        }
    }

    public void przelewWychodzący(double kwota) {
//        synchronized (lock) {
            this.stanKonta -= kwota;
//        }
    }

    public void getStanKonta() {
        System.out.println(stanKonta);
    }
}
