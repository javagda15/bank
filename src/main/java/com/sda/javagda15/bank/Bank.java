package com.sda.javagda15.bank;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {
    private KontoBankowe kontoBankowe;
    private ExecutorService pula = Executors.newCachedThreadPool();

    public Bank() {
        this.kontoBankowe = new KontoBankowe();
    }

    public void przelewWychodzący(double kwota){
        ZleceniePrzelewu zleceniePrzelewu = new ZleceniePrzelewu(kontoBankowe, kwota, KierunekPrzelewu.WYCHODZĄCY);

        // ...
        pula.submit(zleceniePrzelewu);
    }

    public void przelewPrzychodzący(double kwota){
        ZleceniePrzelewu zleceniePrzelewu = new ZleceniePrzelewu(kontoBankowe, kwota, KierunekPrzelewu.PRZYCHODZĄCY);
        // ...
        pula.submit(zleceniePrzelewu);
    }

    public void wypiszStanKonta(){
        kontoBankowe.getStanKonta();
    }
}
