package com.sda.javagda15.bank;

public class ZleceniePrzelewu implements Runnable{

    private KontoBankowe kontoBankowe;
    private double kwota;
    private KierunekPrzelewu kierunekPrzelewu;

    public ZleceniePrzelewu(KontoBankowe kontoBankowe, double kwota, KierunekPrzelewu kierunekPrzelewu) {
        this.kontoBankowe = kontoBankowe;
        this.kwota = kwota;
        this.kierunekPrzelewu = kierunekPrzelewu;
    }

    public void run() {
        if(kierunekPrzelewu == KierunekPrzelewu.PRZYCHODZĄCY){
            kontoBankowe.przelewPrzychodzący(kwota);
        }else if(kierunekPrzelewu == KierunekPrzelewu.WYCHODZĄCY){
            kontoBankowe.przelewWychodzący(kwota);
        }
    }

    // Nr tel: 519 088 399
}
